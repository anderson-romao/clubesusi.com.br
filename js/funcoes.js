// HOME -> planos
$(document).ready(function(){
	$('.carousel-planos').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
		dots: false,
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 3,
			}
		}
		]
	});
});

// HOME -> loja virtual
$(document).ready(function(){
	$('.carousel-loja-virtual').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 10000,
		arrows: true,
		dots: true,
		prevArrow:'<img src="imagens/logos/icon-arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/icon-arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});
});

// HOME -> depoimentos
$(document).ready(function(){
	$('.carousel-depoimentos').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 10000,
		arrows: true,
		dots: true,
		prevArrow:'<img src="imagens/logos/icon-arrow-left-red.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/icon-arrow-right-red.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});
});

// Transição suave entre os menus
$(function() {
	$(".desliza").on("click", function (e) {
    	e.preventDefault();
    	var target = this.hash;
    	var $target = $(target);
    	$("html, body").stop().animate({"scrollTop": ($target.offset().top - ($("header").hasClass("top") ? $("header").height() : 0 ))}, 700, "swing", null);
  	});
});

// Back to top button
var btn = $('#buttonTop');
$(window).scroll(function() {
	if ($(window).scrollTop() > 300) {
    	btn.addClass('show');
  	} else {
    	btn.removeClass('show');
  	}
});
btn.on('click', function(e) {
	e.preventDefault();
  	$('html, body').animate({scrollTop:0}, '300');
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});